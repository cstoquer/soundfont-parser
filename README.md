# Petaporon Instrument Creator

A tool I developed and using to create new instrument banks for [Petaporon](https://pixwlk.itch.io/petaporon).

This tool has been developed with [Pixelbox](https://pixwlk.itch.io/pixelbox).

*Petaporon Instrument Creator* is deployed as a web application on [Itch.io](https://pixwlk.itch.io/petaporon-editor).

# Petaporon instrument format

*Petaporon* is an online piano roll editor available on [itch.io](https://pixwlk.itch.io/petaporon). Various instrument banks are availables. A bank is a set of 10 instruments. Each instrument can be monotimbral (using just one sound) or multitimbral (e.g. drum kits). An instrument range is 3 octaves, which is 36 notes.

The Petaporon instrument format is composed of a single mp3 file gathering all sounds used by the program, and a JSON file describing how samples are assigned to notes of each instruments.

A note has the following parameters:
- `start`, `end` and `loop` points in the audio
- ADSR envelope (Attack, Decay, Sustain, Release) with a Percussion setting (if the option is set, release will occur immediately after decay ends)
- volume control
- pitch
- a single LFO with Delay/Attack envelope
- LFO can control both amplitude (tremolo) and pitch (vibrato)

# Features

- Import audio samples. Supported formats are wav, mp3 and soundfount files (`.sf2`)
- MIDI support to test instruments.
- Exports petaporon program as a single zip file.

# UI

UI is separated into 5 sections:

```
+--------+------------+-----------+
|  menu  |            |instruments|
+--------|            +-----------+
|        | instrument |           |
| sample | editor     | sound     |
|  list  |            | editor    |
|        |            |           |
+--------+------------+-----------+
```

### Menu
- Export petaporon program file
- Save program for later edition. Note: These programs can only be reloaded with the desktop app.
- Export used samples as a single wav file
- Open waveform visualizer to edit loop point of the selected sample

### Sample list

Sample loaded in the UI appears here. To load samples, drag & drop sample files into the UI. Supported formats are wav, mp3 and soundfount files (`.sf2`). Note that soundfont file will appears as a collection of samples. The editor will load loop points from soundfount.

### Instrument editor

Display the 3 octaves that the instrument is composed of. Instrument can be played with the graphic keyboard, or with a MIDI device plugged to your computer (You need to have WebMIDI enabled).

The sample currently selected in the sample list can be assigned to the currently selected note by pressing the `space bar`. And you can remove the sample by pressing `delete`.

### Instruments selector

10 buttons to switch between the 10 intruments of the program.

### Sound editor

Form this editor, you can edit the parametters of a note:
- ADSR envelope
- volume control
- pitch
- "test tone" keyboard to help you tune the sample correctly
- LFO speed, delay and attack
- amplitude modulation (tremolo) and pitch modulation (vibrato)

# Libraries

- [jszip](https://github.com/Stuk/jszip)
- [sf2-parser](https://github.com/colinbdclark/sf2-parser)
- [audio-encoder](https://github.com/cstoquer/audio-encoder)
