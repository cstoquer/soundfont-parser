// small hack to optimize assets
if (!assets.ui) assets.ui = {};
assets.ui.keyboard    = getMap('keyboard');
assets.ui.separator   = getMap('separator');
assets.ui.instruments = getMap('instruments');

var ui = require('./ui');
require('./fileDrop');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
ui.addEditor(require('./ui/menu'));
ui.addEditor(require('./ui/sampleList'));
ui.addEditor(require('./ui/programEditor'));
ui.addEditor(require('./ui/instrumentSelector'));
ui.addEditor(require('./ui/voiceEditor'));

ui.render();

exports.update = ui.update.bind(ui);

window.ui = ui;
window.popup = require('./ui/progressPopup')