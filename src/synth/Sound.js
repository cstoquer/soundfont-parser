// var createFontBuffer = require('../engine/createFontBuffer');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
// Sound is the objects that are sent with note messages to the synth engine.
// similar to petaporon
function Sound(note, noteIndex) {
	this._note  = note; // reference to the note data. It could have a different index
	this._index = noteIndex;
	this.sample = note.sample;
	this.buffer = note.sample.channelData;
	this.pitch  = 48;
	this.vol    = 1;
	this.env    = null; // [A, D, S, R, P]
	this.perc   = false;
	this.lfo    = null; // [Delay, Attack, Speed, Vibrato, Tremolo]
	this.hasLfo = false;

	this.update();
}
module.exports = Sound;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Sound.prototype.update = function () {
	// pitch
	var diffIndex = this._index - this._note.index;
	this.pitch    = 48 + this._note.pitch + diffIndex;

	// volume
	this.vol = this._note.volume / 10;

	// enveloppe
	this.env = this._note.getEnvelopeArray();
	this.lfo = this._note.getLfoArray();

	this.perc   = this._note.p;
	this.hasLfo = this._note.lfo;
};
