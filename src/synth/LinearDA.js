var EG_STATES_D = 1;
var EG_STATES_A = 2;
var EG_STATES_S = 0;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
/**
 * Delay/Attack type of envelope, with linear curves.
 */
function LinearDA() {
	this.state  = EG_STATES_S;
	this.level  = 0;
	this.delay  = 0;
	this.env    = null;
}
module.exports = LinearDA;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
LinearDA.prototype.start = function (env) {
	this.env = env;
	this.level = 0;
	this.delay = 0;
	this.state = EG_STATES_D; // delay
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
/**
 * @returns {Boolean} true if envelope is active (has level > 0)
 */
LinearDA.prototype.update = function () {
	if (this.state === EG_STATES_S) return true;
	var env = this.env;

	switch (this.state) {
		case EG_STATES_D: // delay
			this.delay += env[0]; // attack
			if (this.delay >= 1) {
				// this.delay = 1;
				this.state = EG_STATES_A;
			}
			return false;

		case EG_STATES_A:
			this.level += env[1]; // attack
			if (this.level >= 1) {
				this.level = 1;
				this.state = EG_STATES_S;
			}
			break;
	}
	return true;
};
