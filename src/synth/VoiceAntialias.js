var noteUtils  = require('./noteUtils');
var LinearADSR = require('./LinearADSR');
var LinearDA   = require('./LinearDA');

var BASE_FREQ = noteUtils.noteToFreqTable[60];
var TAU       = Math.PI * 2;
var MAX_AMP   = 0.5;


//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function VoiceAntialias(synth) {
	this.synth    = synth; // reference to the polysynth this voice belongs to.
	this.voiceId  = -1;
	this.listRef  = null;
	this._buffer  = null;
	this._sound   = null;
	this._note    = 0;
	this.stopped  = true;
	this._headPos = 0;
	this._headInc = 0;

	this._envelopeTimer = 0;
	this._controlTimer  = 0;
	this._amp = MAX_AMP;
	this._velocity = 1;

	// LFO
	this._hasLfo = false;
	this._lfoEnv = new LinearDA();
	this._lfoPos = 0; // position
	this._lfoInc = 0; // increment
	this._vibLvl = 0; // vibrato level
	this._trmLvl = 1; // tremolo level

	// envelope
	this.envelope = new LinearADSR();
}
module.exports = VoiceAntialias;


//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
VoiceAntialias.prototype.reset = function () {
	if (this.stopped) return;
	this.synth.freeVoice(this);
	this.stopped = true;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
VoiceAntialias.prototype.noteOn = function (sound, velocity) {
	this._buffer = sound.buffer;
	this._sound  = sound;
	this._note   = sound.pitch;
	this.envelope.setValues(this._sound.env);

	// reset all envelopes and arpeggiator
	this._controlTimer = 0;
	this._headPos      = sound.sample.start;

	this._velocity = velocity || 0;
	this._vibLvl = 0; // vibrato level
	this._trmLvl = 1; // tremolo level
	this._updateFreq();
	this._updateGain();

	this.envelope.start();

	this._hasLfo = sound.hasLfo;
	if (this._hasLfo) {
		this._lfoEnv.start(this._sound.lfo);
		this._lfoPos = 0; // position
		this._lfoInc = 0; // increment
	}

	this.stopped = false;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
VoiceAntialias.prototype.noteOff = function () {
	this.envelope.end();
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
VoiceAntialias.prototype._updateGain = function () {
	this._amp = this._sound.vol * this._trmLvl * MAX_AMP * this._velocity;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
VoiceAntialias.prototype._updateFreq = function () {
	var note = this._note + this._vibLvl;
	this._headInc = noteUtils.noteToFreq(note) / BASE_FREQ / window.OVERSAMPLING;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
VoiceAntialias.prototype._updateLFO = function () {
	var lfo = this._sound.lfo; // [Delay, Attack, Speed, Vibrato, Tremolo]

	this._lfoPos += lfo[2];
	if (this._lfoPos > 1) this._lfoPos -= 1;

	var env = this._lfoEnv.level;
	var level = Math.sin(this._lfoPos * TAU);
	this._vibLvl = lfo[3] * level * env;
	this._trmLvl = 1 - lfo[4] * (level + 1) * env;

	this._updateFreq();
	this._updateGain();
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
VoiceAntialias.prototype.generate = function (bufferL, bufferR, start, end) {
	if (this.stopped) return;

	for (var t = start; t < end; t++) {
		// step envelope signals
		if (++this._envelopeTimer >= 64) {
			this._envelopeTimer = 0;
			if (this.envelope.update()) {
				this.stopped = true;
				this.synth.freeVoice(this);
				return;
			}
			if (this._hasLfo && this._lfoEnv.update()) {
				this._updateLFO();
			}
		}

		// step head
		var out = 0;
		for (var a = 0; a < window.OVERSAMPLING; a++) {
			this._headPos += this._headInc;


			if (this._headPos >= this._sound.sample.end) {
				if (this._sound.sample.loop === 0) {
					this.stopped = true;
					this.synth.freeVoice(this);
					return;
				}
				this._headPos -= this._sound.sample.loop;
			}

			// TODO: interpolation and mip-mapping
			out += this._buffer[~~this._headPos];
		}

		out = out * this.envelope.level * this._amp / window.OVERSAMPLING;


		// mix output in buffer
		bufferL[t] += out;
		bufferR[t] += out;
	}
};
