var Voice = require('./VoiceAntialias');
var List  = require('container-doublylist');

var POLYPHONY = 8;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function PolySynth() {
	this.voiceId = 0;
	this.voices  = [];
	this.free    = [];
	this.active  = new List();

	for (var i = 0; i < POLYPHONY; i++) {
		var voice = new Voice(this);
		this.voices.push(voice);
		this.free.push(voice);
	}
}
module.exports = PolySynth;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
PolySynth.prototype.freeVoice = function (voice) {
	this.active.removeByReference(voice.listRef);
	voice.listRef = null;
	this.free.push(voice);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
PolySynth.prototype.noteOn = function (sound, velocity) {
	// get voice
	var voice;
	if (this.free.length > 0) {
		voice = this.free.pop();
	} else {
		// FIXME: find the best suitable voice. currently default to oldest
		voice = this.active.popBack();
	}

	// prepare voice
	voice.voiceId = this.voiceId++;
	voice.listRef = this.active.addFront(voice);
	voice.noteOn(sound, velocity);

	return voice;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
PolySynth.prototype.stop = function () {
	for (var i = 0; i < this.voices.length; i++) {
		this.voices[i].reset();
	}
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
PolySynth.prototype.generate = function (bufferL, bufferR, start, end) {
	for (var node = this.active.first; node !== null; node = node.next) {
		var voice = node.object;
		voice.generate(bufferL, bufferR, start, end);
	}
};
