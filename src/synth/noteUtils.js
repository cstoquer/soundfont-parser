function noteToFreq(midiNoteNumber) {
	return 440 * Math.pow(2, (midiNoteNumber - 69) / 12);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
// Keep a table with frequency for all MIDI notes, for fast access.
var noteToFreqTable = [];

for (var i = 0; i < 128; i++) noteToFreqTable.push(noteToFreq(i));
// Object.freeze(noteToFreqTable);

exports.noteToFreqTable = noteToFreqTable;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
/**
 * Return oscillator frequency from (float) note value.
 */
exports.noteToFreq = function(note) {
	if (note <   0) note = 0;
	if (note > 127) note = 127;

	var c  = ~~note;
	var d  = note - c;
	var f1 = noteToFreqTable[c];
	var f2 = noteToFreqTable[c + 1];

	return f1 + d * (f2 - f1);
};
