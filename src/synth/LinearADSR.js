var EG_STATES_A = 4;
var EG_STATES_D = 3;
var EG_STATES_S = 2;
var EG_STATES_R = 1;
var EG_STATES_O = 0;

var DEFAULT_ENV = [0, 0, 0, 0];

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
/**
 * Attack/Decay/Sustain/Release type of envelope, with linear curves.
 */
function LinearADSR() {
	this.state  = EG_STATES_O;
	this.level  = 0;
	this.env    = DEFAULT_ENV;
	this.isPerc = false;
}
module.exports = LinearADSR;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
LinearADSR.prototype.setValues = function (env) {
	this.env = env;
	this.isPerc = !!env[4];
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
LinearADSR.prototype.start = function () {
	this.level = 0;
	this.state = EG_STATES_A;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
LinearADSR.prototype.end = function () {
	if (this.isPerc) return; // perc mode, noteOff does nothing
	this.state = EG_STATES_R;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
/**
 * @returns {Boolean} true if envelope stopped durting this update
 */
LinearADSR.prototype.update = function () {
	if (this.state === EG_STATES_O) return false;
	var env = this.env;

	switch (this.state) {
		case EG_STATES_S: // sustain
			break;

		case EG_STATES_A:
			this.level += env[0]; // attack
			if (this.level >= 1) {
				this.level = 1;
				this.state = EG_STATES_D;
			}
			break;

		case EG_STATES_D:
			this.level -= env[1]; // decay
			if (this.level <= env[2]) {
				this.level = env[2]; // sustain
				if (this.isPerc) {
					// percussive envelope
					this.state = EG_STATES_R;
				} else {
					// sustained envelope
					this.state = EG_STATES_S;
				}
			}
			break;

		case EG_STATES_R:
			this.level -= env[3]; // release
			if (this.level <= 0) {
				this.level = 0;
				this.state = EG_STATES_O;
				return true;
			}
			break;
	}

	return false;
};
