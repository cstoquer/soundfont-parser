var audioContext = require('../engine/audioContext');
var PolySynth    = require('./PolySynth');

var BUFFER_LENGTH = 512; //2048;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function Generator() {
	this.synth       = new PolySynth(this);
	this._gainNode   = null;
	this._scriptNode = null;
	this.buffL       = null;
	this.buffR       = null;

	this._notePlaying = {};
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Generator.prototype.setAsGenerator = function () {
	if (this._scriptNode) return;

	this._gainNode = audioContext.createGain();
	this._gainNode.connect(audioContext.destination);
	this._gainNode.gain.value = 0.5;

	var scriptNode = audioContext.createScriptProcessor(BUFFER_LENGTH, 0, 2);
	scriptNode.onaudioprocess = this.onAudioProcess.bind(this);
	scriptNode.connect(this._gainNode);

	this._scriptNode = scriptNode;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Generator.prototype.onAudioProcess = function (event) {
	var outputBuffer = event.outputBuffer;
	var buffL = this.buffL = outputBuffer.getChannelData(0);
	var buffR = this.buffR = outputBuffer.getChannelData(1);

	// clear buffers content
	for (var i = 0; i < BUFFER_LENGTH; i++) {
		buffL[i] = 0;
		buffR[i] = 0;
	}

	this.synth.generate(this.buffL, this.buffR, 0, BUFFER_LENGTH);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Generator.prototype.noteOn = function (note, sound, velocity) {
	if (this._notePlaying[note]) return;

	var voice = this.synth.noteOn(sound, velocity);

	this._notePlaying[note] = {
		voice: voice,
		voiceId: voice.voiceId
	};
};

Generator.prototype.noteOff = function (note) {
	if (!this._notePlaying[note]) return;
	var playing = this._notePlaying[note];

	if (playing.voice.voiceId === playing.voiceId) {
		playing.voice.noteOff();
	}

	delete this._notePlaying[note];
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
module.exports = Generator;
