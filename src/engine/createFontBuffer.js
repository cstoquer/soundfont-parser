var maximizeAudio = require('./maximizeAudio');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function createFontBuffer(soundfont, sampleIndex) {
	var samples     = soundfont.sample[sampleIndex];
	var sampleMeta  = soundfont.sampleHeader[sampleIndex];
	var audioBuffer = maximizeAudio(samples);

	sampleMeta.buffer      = audioBuffer;
	sampleMeta.channelData = audioBuffer.getChannelData(0);

	return audioBuffer;
}

module.exports = createFontBuffer;
