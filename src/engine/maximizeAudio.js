var audioContext = require('./audioContext');


//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function maximizeAudio(samples) {
	var length      = samples.length;
	var audioBuffer = audioContext.createBuffer(1, length, 44100);
	var channelData = audioBuffer.getChannelData(0);

	var peak = 0;
	for (var i = 0; i < length; i++) {
		var sample = samples[i];
		if (Math.abs(sample) > peak) peak = Math.abs(sample);
	}

	for (var i = 0; i < length; i++) {
		var sample = samples[i] / peak;
		channelData[i] = sample;
	}

	return audioBuffer;
}

module.exports = maximizeAudio;
