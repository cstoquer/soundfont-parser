var audioContext  = require('./audioContext');
var maximizeAudio = require('./maximizeAudio');


var AUDIO_BUFFER_CACHE = {};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
/** Making sure that if the same audio is requested several times, only one
 * request is initiated and all callbacks triggers.
 */
function BufferCache (file) {
	var isFile = file instanceof File;
	this.file     = isFile ? file : null;
	this.uri      = isFile ? file.path : file;
	this.buffer   = null;
	this.error    = null;
	this.loading  = false;
	this.onLoad   = [];
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
BufferCache.prototype.load = function (cb) {
	if (this.error || this.buffer) {
		return cb(this.error, this.buffer);
	}
	this.onLoad.push(cb);

	if (!this.loading) {
		this.initiateLoad();
	}
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
BufferCache.prototype.initiateLoad = function () {
	this.loading = true;

	var self = this;

	if (this.file) {
		var reader = new FileReader();
		reader.onload = function (e) {
			self.decodeData(e.target.result);
		};

		reader.readAsArrayBuffer(this.file);
	} else {
		// FIXME: remove dependency to fs
		var fs = require('fs');
		fs.readFile(this.uri, function (error, data) {
			if (error) return self.completeLoad(error);
			self.decodeData(data.buffer);
		});
	}
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
BufferCache.prototype.decodeData = function (buffer) {
	var self = this;
	audioContext.decodeAudioData(buffer, function onSuccess(audioBuffer) {
		audioBuffer = maximizeAudio(audioBuffer.getChannelData(0));
		return self.completeLoad(null, audioBuffer);
	}, self.completeLoad.bind(self));
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
BufferCache.prototype.completeLoad = function (error, buffer) {
	this.buffer = buffer;
	this.error  = error;

	for (var i = 0; i < this.onLoad.length; i++) {
		this.onLoad[i](error, buffer);
	}

	this.onLoad = [];
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
module.exports = function (file, cb) {
	var isFile = file instanceof File;
	var id = file; // default: file is a uri string.
	if (isFile) {
		if (file.path) {
			id = file.path;
		} else {
			// NOTE: allow several files with the same name to be loaded.
			id = file.name + ':' + file.size;
		}
	}

	var bufferCache = AUDIO_BUFFER_CACHE[id];
	if (!bufferCache) {
		bufferCache = AUDIO_BUFFER_CACHE[id] = new BufferCache(file);
	}

	bufferCache.load(cb);
};
