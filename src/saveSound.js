var audioContext = require('./engine/audioContext');
var FileSaver    = require('file-saver');

var SAMPLE_RATE = 44100;


function onProgress(percent) {
	//
}

module.exports = function () {
	var length = 44100; // 1 second
	var buffer = audioContext.createBuffer(1, length, SAMPLE_RATE);
	var channelData = buffer.getChannelData(0);

	// fill buffer with random stuff
	for (var i = 0; i < length; i++) {
		channelData[i] = Math.cos(i * 0.05);
	}

	audioEncoder(buffer, 0, onProgress, function onDone(blob) {
		FileSaver.saveAs(blob, 'test.wav');
	});
};
