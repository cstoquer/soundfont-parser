var Program = require('./data/Program');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var state = {
	samples:         [],
	sampleIndex:     0,
	instrumentIndex: 0,
	noteIndex:       0,
	program:         null,
};

state.program = new Program(state);

module.exports = state;
