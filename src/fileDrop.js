var state       = require('./state');
var ui          = require('./ui');
var soundfonts  = require('./data/soundfonts');
var Sample      = require('./data/Sample');
var persistency = require('./persistency');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function openFiles(e) {
	var files = e.dataTransfer.files;
	var file  = files[0]; // TODO: process all files
	var res   = /^(.*)\.(wav|WAV|mp3|MP3|sf2|SF2|petaprog)$/.exec(file.name || file.path);
	if (!res) return console.error('Unsupported extension');

	var ext   = res[2];
	var name  = res[1];

	name = name.split('/').pop();
	name = name.split('\\').pop();

	switch (ext.toLowerCase()) {
		case 'sf2':
			soundfonts.loadSoundfont(file, function () { ui.render(); });
			break;

		case 'wav':
		case 'mp3':
			var sample = new Sample();
			state.samples.push(sample);
			sample.fromDataTransfer(name, file, function () { ui.render(); });
			break;

		case 'petaprog':
			persistency.loadProgram(file);
			ui.render();
			break;

		default:
			console.log('Unsupported file', file)
	}
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
// drag & drop patch files
document.body.addEventListener('dragover', function handleDragOver(e) {
	e.stopPropagation();
	e.preventDefault();
	e.dataTransfer.dropEffect = 'copy';
}, false);

document.body.addEventListener('drop', function (e) {
	e.stopPropagation();
	e.preventDefault();
	openFiles(e);
}, false);

document.addEventListener('drop', function (e) {
	e.preventDefault();
	e.stopPropagation();
	openFiles(e);
});

document.addEventListener('dragover', function (e) {
	e.preventDefault();
	e.stopPropagation();
});
