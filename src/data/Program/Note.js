var roundToSignificantDigits = require('./roundToSignificantDigits');
var ENVELOPE_VALUES          = require('./envelopeValues');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function Note(index, sample) {
	this.sample      = sample;
	this.index       = index;       // note index in instrument (0..35);
	// this.sampleName  = sample.name;
	this.pitch       = 0;
	this.volume      = 10;
	this.a           = 10;
	this.d           = 100;
	this.s           = 800;
	this.r           = 150;
	this.p           = false;
	this.lfo         = false;
	this.delay       = 250;
	this.raise       = 400;
	this.speed       = 40;
	this.vibrato     = 20;
	this.tremolo     = 0;
}
module.exports = Note;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Note.prototype.getEnvelopeArray = function () {
	// convert note's [0..999] to [1.000..0.001] ranges
	// NOTE: 1.000 is fast, 0.001 is slow
	return [
		ENVELOPE_VALUES[this.a],
		ENVELOPE_VALUES[this.d],
		roundToSignificantDigits(this.s / 999),
		ENVELOPE_VALUES[this.r],
		(~~this.p)
	];
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Note.prototype.getLfoArray = function () {
	return [
		ENVELOPE_VALUES[this.delay],
		ENVELOPE_VALUES[this.raise],
		roundToSignificantDigits((this.speed + 2) / 5000), // TUNE ME
		roundToSignificantDigits(this.vibrato / 99),
		roundToSignificantDigits(this.tremolo / 300), // TUNE ME, must be in [0..0.5]
	];
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Note.prototype.render = function () {
	// var y = 35 - this.index;
	// locate(40, y);
	// pen(9).print('[' + this.sampleIndex + '] ');
	pen(1).print(this.sample.name, 160, 211 - this.index * 6);
	// pen(11).print(this.pitch);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Note.prototype.setPitch = function (pitch) {
	this.pitch = pitch;
	// TODO: update sound
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Note.prototype.setEnvelope = function (attribute, value) {
	this[attribute] = value;
	// TODO: update sound
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Note.prototype.setPerc = function (value) {
	this.p = value;
	// TODO: update sound
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Note.prototype.setVolume = function (value) {
	this.volume = value;
	// TODO: update sound
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Note.prototype.serialize = function (samples) {
	var data = {};
	data.sample  = samples.indexOf(this.sample);
	data.pitch   = this.pitch;
	data.volume  = this.volume;
	data.a       = this.a;
	data.d       = this.d;
	data.s       = this.s;
	data.r       = this.r;
	data.p       = this.p;
	data.lfo     = this.lfo;
	data.delay   = this.delay;
	data.raise   = this.raise;
	data.speed   = this.speed;
	data.vibrato = this.vibrato;
	data.tremolo = this.tremolo;
	return data;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Note.prototype.deserialize = function (data, samples) {
	// assuming samples list is initialized properly
	this.sample  = samples[data.sample];
	this.pitch   = data.pitch;
	this.volume  = data.volume;
	this.a       = data.a;
	this.d       = data.d;
	this.s       = data.s;
	this.r       = data.r;
	this.p       = data.p;
	this.lfo     = data.lfo;
	this.delay   = data.delay;
	this.raise   = data.raise;
	this.speed   = data.speed;
	this.vibrato = data.vibrato;
	this.tremolo = data.tremolo;
	return this;
};
