//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
// keep only 3 significant digits
function roundToSignificantDigits(value) {
	var s = 1e3;
	     if (value < 0.001) s = 1e6;
	else if (value < 0.01)  s = 1e5;
	else if (value < 0.1)   s = 1e4;

	value = Math.round(value * s) / s;
	return Math.min(1, value);
}

module.exports = roundToSignificantDigits;
