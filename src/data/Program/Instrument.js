var Note  = require('./Note');
var Sound = require('../../synth/Sound');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
// an instrument is a collection of 36 notes.
// notes are grouped in cluster for which is defined a sample, pitch and envelope
function Instrument(program) {
	this.program = program;
	this.notes = [];
	for (var i = 0; i < 36; i++) {
		this.notes.push(null);
	}
}
module.exports = Instrument;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Instrument.prototype.setNote = function (index, sample) {
	this.notes[index] = new Note(index, sample);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Instrument.prototype.deleteNote = function (index) {
	this.notes[index] = null;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Instrument.prototype.render = function () {
	for (var i = 0; i < 36; i++) {
		var note = this.notes[i];
		if (!note) {
			continue;
		}
		note.render();
	}
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Instrument.prototype.getNote = function (index) {
	var i = index;
	while (i >= 0) {
		var note = this.notes[i--];
		if (note) return note;
	}
	return null;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Instrument.prototype.getSoundForSynth = function (noteIndex) {
	var note = this.getNote(noteIndex);
	if (!note) return null;
	var sound = new Sound(note, noteIndex); // TODO: put in cache
	return sound;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Instrument.prototype.serialize = function () {
	var samples = this.program.getSamples();
	var data = [];
	for (var i = 0; i < this.notes.length; i++) {
		var note = this.notes[i];
		if (!note) {
			data.push(0);
		} else {
			data.push(note.serialize(samples));
		}
	}
	return data;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Instrument.prototype.deserialize = function (data) {
	var samples = this.program.getSamples();
	this.notes = [];
	for (var i = 0; i < data.length; i++) {
		var noteData = data[i];
		if (noteData) {
			var note = new Note(i);
			note.deserialize(noteData, samples);
			this.notes.push(note);
		} else {
			this.notes.push(null);
		}
	}
	return this;
};
