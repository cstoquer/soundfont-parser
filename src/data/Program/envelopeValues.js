var roundToSignificantDigits = require('./roundToSignificantDigits');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var ENVELOPE_DURATIONS = [0.00002267, 0.0005, 0.001, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08];
var envelopeValues = [];

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function createEnvelopeValues() {
	for (var i = 1; i < ENVELOPE_DURATIONS.length; i++) {
		var lo = ENVELOPE_DURATIONS[i - 1];
		var hi = ENVELOPE_DURATIONS[i];
		for (var j = 0; j < 100; j++) {
			var duration = lo + j * (hi - lo) / 100;
			if (duration === 0) {
				envelopeValues.push(1);
				continue;
			}
			var increment = roundToSignificantDigits(1 / (44100 * duration));
			envelopeValues.push(increment);
		}
	}
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
createEnvelopeValues();
module.exports = envelopeValues;
