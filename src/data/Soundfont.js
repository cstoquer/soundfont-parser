var Sample = require('./Sample');


//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
/* soundfont = {
	instrument: […]
	instrumentZone […]
	instrumentZoneGenerator: (218) […]
	instrumentZoneModulator: (226) […]
	parserOptions: undefined
	presetHeader: (28) […]
	presetZone: (29) […]
	presetZoneGenerator: (29) […]
	presetZoneModulator: […]
	sample: Int16Array[]
	sampleHeader: SampleHeader[] -> { sampleName, sampleRate:, sampleType (channelCount?), sampleLink (=sampleId), startLoop, endLoop, originalPitch, pitchCorrection }
	samplingData: b.Riff.Chunk {type: "smpl", size: 1605746, offset: 202}
} */

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function Soundfont(uri) {
	this.uri      = uri;
	this.parser   = null;
	this.samples  = [];
	this._loading = false;
	// this.name    = path.basename(uri, '.sf2');
	// this.load();
}
module.exports = Soundfont;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Soundfont.prototype.load = function (cb) {
	if (this._loading) return cb && cb('alreadyLoaded');
	this._loading = true;
	// FIXME: remove dependency to fs
	var fs = require('fs');
	var sf2Data = fs.readFileSync(this.uri, null);
	this.parser = new sf2.Parser(sf2Data);
	this._parse();
	cb && cb();
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Soundfont.prototype.loadFromFile = function (file, cb) {
	if (this._loading) return cb && cb('alreadyLoaded');
	this._loading = true;

	var self = this;

	var reader = new FileReader();
	reader.onload = function (e) {
		var sf2Data = new Uint8Array(e.target.result);
		var parser = new sf2.Parser(sf2Data);
		self.parser = parser;
		self._parse();
		cb && cb();
	};

	reader.readAsArrayBuffer(file);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Soundfont.prototype._parse = function () {
	this.parser.parse();
	var nSamples = this.parser.sample.length;
	nSamples -= 1; // remove EOF sample

	for (var i = 0; i < nSamples; i++) {
		var sample = new Sample();
		sample.fromSoundfont(this, i);
		this.samples.push(sample);
	}
};
