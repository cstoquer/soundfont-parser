var loadAudioBuffer  = require('../engine/loadAudioBuffer');
var createFontBuffer = require('../engine/createFontBuffer');
var soundfonts       = require('./soundfonts');

var CLEANUP_SOUNDFONT_NAME = /([a-zA-Z0-9_\.\- ]*).*/;


//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function Sample() {
	this.name        = '';
	this.id          = '';
	this.audioBuffer = null;  // audioBuffer
	this.channelData = null;  // channel data
	this.length      = 0;
	this.sampleRate  = 44100;
	this.start       = 0;
	this.end         = 0;
	this.loop        = 0;
	this.uri         = '';   // file uri (wav, mp3 or soundfont file)
	this.soundfont   = null; // if a sample from a soundfont, reference to it
	this.sampleIndex = -1;   // if a sample from a soundfont, index of the sample
}
module.exports = Sample;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Sample.prototype.fromUri = function (name, uri, cb) {
	this.name = name;
	this.uri  = uri;
	this.id   = uri;
	this._loadAudioBuffer(uri, cb);
	return this;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Sample.prototype.fromDataTransfer = function (name, file, cb) {
	this.name = name;
	this.uri  = file.path;
	this.id   = file.name || file.path;
	this._loadAudioBuffer(file, cb);
	return this;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Sample.prototype._loadAudioBuffer = function (uri, cb) {
	var self = this;

	loadAudioBuffer(uri, function (error, buffer) {
		if (error) {
			console.error(error);
			return cb && cb(error);
		}

		var channelData = buffer.getChannelData(0);
		self.audioBuffer = buffer;
		self.channelData = channelData
		self.length = self.end = channelData.length;

		cb && cb();
	});
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Sample.prototype.fromSoundfont = function (soundfont, sampleIndex) {
	this.uri       = soundfont.uri;
	this.soundfont = soundfont;
	this.sampleIndex = sampleIndex;

	var soundfontParser = soundfont.parser;
	// this.sampleData = soundfontParser.sample[sampleIndex];
	var sampleHeader = soundfontParser.sampleHeader[sampleIndex];

	this.name = sampleHeader.sampleName;

	var clean = (CLEANUP_SOUNDFONT_NAME.exec(this.name));
	if (clean && clean[1]) {
		this.name = clean[1];
	}

	this.id = soundfont.name + '/' + sampleIndex;

	if (!sampleHeader.channelData) {
		createFontBuffer(soundfontParser, sampleIndex);
	}
	this.audioBuffer = sampleHeader.buffer;
	this.channelData = sampleHeader.channelData;
	this.length      = sampleHeader.channelData.length;

	this.sampleRate = 44100;

	// sample markers points
	var end  = sampleHeader.endLoop;
	var loop = 0;

	if (end === 0) end = this.channelData.length;
	if (sampleHeader.startLoop !== 0) loop = sampleHeader.endLoop - sampleHeader.startLoop;

	this.start = 0;
	this.end   = end;
	this.loop  = loop;

	return this;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Sample.prototype.serialize = function () {
	var data = {
		uri: this.uri,
		s:   this.start,
		e:   this.end,
		l:   this.loop,
	};

	if (this.soundfont) {
		data.soundfont = true;
		data.index = this.sampleIndex;
	} else {
		data.name = this.name;
	}
	return data;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Sample.prototype.deserialize = function (data) {
	if (data.soundfont) {
		var soundfont = soundfonts.getSoundfont(data.uri);
		this.fromSoundfont(soundfont, data.index);
	} else {
		this.fromUri(data.name, data.uri);
	}

	this.start = data.s;
	this.end   = data.e;
	this.loop  = data.l;

	return this;
};
