var Soundfont = require('./Soundfont');
var state     = require('../state');

var SOUNDFONTS_CACHE = {};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function getSoundfont(id) {
	if (SOUNDFONTS_CACHE[id]) return SOUNDFONTS_CACHE[id];
	var soundfont = new Soundfont(id);
	SOUNDFONTS_CACHE[id] = soundfont;
	return soundfont;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
// static method to retrieve soundfonts in cache or load
function loadSoundfont(file, cb) {
	var id = file.name || file.path;
	var soundfont = getSoundfont(id);

	soundfont.loadFromFile(file, function (error) {
		if (error) {
			cb && cb(error);
			return console.error(error);
		}
		var samples = soundfont.samples;
		for (var i = 0; i < samples.length; i++) {
			state.samples.push(samples[i]);
		}
		cb && cb();
	});

	return soundfont;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function clearCache() {
	SOUNDFONTS_CACHE = {};
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.getSoundfont  = getSoundfont;
exports.loadSoundfont = loadSoundfont;
exports.clearCache    = clearCache;
