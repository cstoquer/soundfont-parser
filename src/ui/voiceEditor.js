var state      = require('../state');
var testTones  = require('../testTones');
var ButtonPool = require('./components/ButtonPool');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var POSX       = 264;
var POSY       = 35;
var BACKGROUND = getMap('envelope');
var VOL_BACK   = getMap('volume');
var PITCH_BACK = getMap('pitch');
var LFO_BACK   = getMap('lfo');

var ENV_LABELS = ['a', 'd', 's', 'r'];
var LFO_LABELS = ['delay', 'raise'];

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var buttons = new ButtonPool();

function onEnvelopeBtn(label, digit, sign) {
	var note = state.program.getNote();
	if (!note) return;
	var unit = Math.pow(10, digit);
	var value = sign * unit;
	note.setEnvelope(label, clamp(note[label] + value, 0, 999));
}

function onPercBtn() {
	var note = state.program.getNote();
	if (!note) return;
	note.setPerc(!note.p);
}

function onVolumeBtn(value) {
	var note = state.program.getNote();
	if (!note) return;
	note.setVolume(value);
}

function onPitchBtn(value) {
	var note = state.program.getNote();
	if (!note) return;
	var pitch = clamp(note.pitch + value, -99.99, 99.99);
	pitch = Math.round(pitch * 100) / 100
	note.setPitch(pitch);
}

function addPitchButtons(x, pitch) {
	buttons.addButton(x, POSY + 78, 8, 12, onPitchBtn.bind(this,  pitch));
	buttons.addButton(x, POSY + 94, 8, 12, onPitchBtn.bind(this, -pitch));
}

function onPianoClick(noteIndex) {
	testTones.noteOn(noteIndex);
}

function onLfoBtn() {
	var note = state.program.getNote();
	if (!note) return;
	note.lfo = !note.lfo;
}

function onLabelBtn(label, increment) {
	var note = state.program.getNote();
	if (!note) return;
	note[label] = clamp(note[label] + increment, 0, 99);
}

function add2DigitButtons(label, x, y) {
	buttons.addButton(x, y - 8, 8, 12, onLabelBtn.bind(this, label,  10));
	buttons.addButton(x, y + 8, 8, 12, onLabelBtn.bind(this, label, -10));
	x += 8;
	buttons.addButton(x, y - 8, 8, 12, onLabelBtn.bind(this, label,  1));
	buttons.addButton(x, y + 8, 8, 12, onLabelBtn.bind(this, label, -1));
}

function createButtons() {
	// envelope
	for (var l = 0; l < ENV_LABELS.length; l++) {
		var label = ENV_LABELS[l];
		for (var i = 0; i < 3; i++) {
			var x = POSX + 8 + l * 32 + i * 8;
			buttons.addButton(x, POSY + 10, 8, 12, onEnvelopeBtn.bind(this, label, 2 - i, 1));
			buttons.addButton(x, POSY + 24, 8, 12, onEnvelopeBtn.bind(this, label, 2 - i, -1));
		}
	}

	// perc
	buttons.addButton(POSX + 6, POSY + 38, 32, 12, onPercBtn.bind(this));

	// volume
	for (var i = 1; i <= 10; i++) {
		buttons.addButton(POSX + 40 + i * 8, POSY + 38, 8, 12, onVolumeBtn.bind(this, i));
	}

	// pitch
	addPitchButtons(POSX +  8, 12);
	addPitchButtons(POSX + 16, 10);
	addPitchButtons(POSX + 24,  1);
	addPitchButtons(POSX + 40, 0.1);
	addPitchButtons(POSX + 48, 0.01);

	// piano (test tone)
	for (var i = 0; i < 7; i++) {
		buttons.addButton(POSX + 72 + i * 8, POSY + 80, 8, 24, onPianoClick.bind(this, i));
	}

	// lfo
	buttons.addButton(POSX + 6, POSY + 118, 32, 12, onLfoBtn.bind(this));
	for (var l = 0; l < LFO_LABELS.length; l++) {
		var label = LFO_LABELS[l];
		for (var i = 0; i < 3; i++) {
			var x = POSX + 8 + l * 32 + i * 8;
			buttons.addButton(x, POSY + 142, 8, 12, onEnvelopeBtn.bind(this, label, 2 - i, 1));
			buttons.addButton(x, POSY + 158, 8, 12, onEnvelopeBtn.bind(this, label, 2 - i, -1));
		}
	}

	add2DigitButtons('speed',   POSX +  80, POSY + 150);
	add2DigitButtons('vibrato', POSX + 112, POSY + 126);
	add2DigitButtons('tremolo', POSX + 112, POSY + 158);
}

createButtons();

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function renderEnvelopeValue(value, x, y) {
	value = ('000' + value).substr(-3);
	for (var i = 0; i < 3; i++) {
		var c = value.charAt(i);
		sprite(c, x + i * 8, y);
	}
}

function drawDigit(value, x, y) {
	var digit = ~~(value % 10);
	sprite(digit, x, y);
}

function draw2digits(value, x, y) {
	sprite(~~(value / 10), x, y);
	sprite(~~(value % 10), x + 8, y);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function render() {
	paper(4).rectf(POSX, POSY, 136, 217 - POSY);
	draw(BACKGROUND, POSX + 8, POSY + 4);
	pen(14);
	var note = state.program.getNote();

	// envelope
	for (var i = 0; i < ENV_LABELS.length; i++) {
		var label = ENV_LABELS[i];
		if (note) renderEnvelopeValue(note[label], POSX + 8 + i * 32, POSY + 20);
	}

	// perc
	print('PERC', POSX + 19, POSY + 42);
	var perc = note && note.p ? 11 : 10;
	sprite(perc, POSX + 8, POSY + 40);

	// volume
	draw(VOL_BACK, POSX + 40, POSY + 40);

	if (note) {
		for (var i = 0; i < note.volume; i++) {
			sprite(29, POSX + 48 + i * 8, POSY + 40);
		}
	}

	// pitch
	draw(assets.ui.separator, POSX, POSY + 60);
	print('PITCH', POSX + 22, POSY + 70);
	print('TEST TONE', POSX + 82, POSY + 70);
	draw(PITCH_BACK, POSX + 8, POSY + 72);

	if (note) {
		sprite(note.pitch < 0 ? 28 : 27, POSX + 8, POSY + 88);
		var pitch = Math.round(Math.abs(note.pitch) * 100);
		drawDigit(pitch / 1000, POSX + 16, POSY + 88);
		drawDigit(pitch /  100, POSX + 24, POSY + 88);
		drawDigit(pitch /   10, POSX + 40, POSY + 88);
		drawDigit(pitch /    1, POSX + 48, POSY + 88);
	}

	// lfo
	draw(assets.ui.separator, POSX, POSY + 112);
	draw(LFO_BACK, POSX + 8, POSY + 120);
	var lfo = note && note.lfo ? 11 : 10;
	sprite(lfo, POSX + 8, POSY + 120);

	for (var i = 0; i < ENV_LABELS.length; i++) {
		var label = LFO_LABELS[i];
		if (note) renderEnvelopeValue(note[label], POSX + 8 + i * 32, POSY + 152);
	}

	if (note) {
		draw2digits(note.speed,   POSX +  80, POSY + 152);
		draw2digits(note.vibrato, POSX + 112, POSY + 128);
		draw2digits(note.tremolo, POSX + 112, POSY + 160);
	}
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.onClick = function (x, y) {
	var note = state.program.getNote();
	if (!note) return;
	if (buttons.click(x, y)) render();
};

exports.onMouseUp = function () {
	testTones.noteOff();
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.rect = { left: 264, right: 400, top: POSY, bottom: 217 };
exports.render = render;
