var state      = require('../state');
var ButtonPool = require('./components/ButtonPool');
var ui         = require('.');

var TOP    = 30;
var HEIGHT = 192;
var HALF   = HEIGHT / 2;

var SAMPLE_BG = getMap('samples');
var currentLabel = 'loop';

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function closePopup() {
	ui.closePopup();
}

function onClick(label, digit) {
	currentLabel = label;
	var sample = state.samples[state.sampleIndex];
	sample[label] += digit;

	sample.start = clamp(sample.start, 0, sample.length);
	sample.end   = clamp(sample.end,   0, sample.length);
	sample.loop  = clamp(sample.loop,  0, sample.length);

	render();
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var buttons = new ButtonPool();

function createMarkerEditor(x, y, label) {
	for (var i = 0; i < 7; i++) {
		var digit = Math.pow(10, 6 - i);
		buttons.addButton(x + i * 8, y -  3, 8, 12, onClick.bind(this, label,  digit));
		buttons.addButton(x + i * 8, y + 11, 8, 12, onClick.bind(this, label, -digit));
	}
}

createMarkerEditor(40,  5, 'start');
createMarkerEditor(112, 5, 'end');
createMarkerEditor(184, 5, 'loop');

buttons.addButton(4, 7, 16, 16, closePopup);


//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function renderBackground() {
	paper(4).rectf(0, 0, 400, 30);
	draw(SAMPLE_BG, 0, 3);

	paper(0).rectf(0, TOP, 400, HEIGHT);
	paper(3).rectf(0, TOP + HALF, 400, 1); // zero line
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function renderLoop() {
	renderBackground();
	var sample = state.samples[state.sampleIndex];
	if (!sample) return;
	var data = sample.channelData;

	// first half: until sample.end
	paper(7);
	for (var i = 0; i < 200; i++) {
		var s = data[sample.end - 200 + i];
		s = Math.round(s * HALF) + HALF;
		rectf(i, TOP + s, 1, 1);
	}

	paper(9).rectf(200, TOP, 1, HEIGHT); // separation

	// second half: after loop
	paper(14);
	for (var i = 0; i < 200; i++) {
		var s = data[sample.end - sample.loop + i];
		s = Math.round(s * HALF) + HALF;
		rectf(i + 200, TOP + s, 1, 1);
	}
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function renderStart() {
	renderBackground();
	var sample = state.samples[state.sampleIndex];
	if (!sample) return;
	var data = sample.channelData;

	paper(7);
	for (var i = 0; i < 400; i++) {
		var s = data[sample.start + i];
		s = Math.round(s * HALF) + HALF;
		rectf(i, TOP + s, 1, 1);
	}
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function drawNumber(x, y, value) {
	var str = ('0000000' + value).substr(-7);
	for (var i = 0; i < 7; i++) {
		sprite(str[i], x + i * 8, y);
	}
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function render() {
	if (currentLabel === 'start') {
		renderStart();
	} else {
		renderLoop();
	}

	var sample = state.samples[state.sampleIndex];
	if (!sample) return;
	drawNumber( 40, 11, sample.start);
	drawNumber(112, 11, sample.end);
	drawNumber(184, 11, sample.loop);
	drawNumber(256, 11, sample.length);

	// buttons.debug();
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.render = render;
exports.onClick = buttons.click.bind(buttons);
