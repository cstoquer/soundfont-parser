var pointer = require('pixelbox/pointer');
var infoTip = require('./infoTip');
var state   = require('../state');
var program = state.program;

var editors = [];
var popup   = null;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.addEditor = function (editor) {
	editors.push(editor);
};

function isInRect(editor, x, y) {
	var rect = editor.rect;
	if (!rect) return false;
	if (x >= rect.left && x <= rect.right && y >= rect.top && y <= rect.bottom) {
		return true
	}
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var mouse = { x: 0, y: 0 };

pointer.onPress(function (x, y) {
	if (popup) {
		popup.onClick && popup.onClick(x, y);
		return;
	}

	for (var i = 0; i < editors.length; i++) {
		var editor = editors[i];
		if (isInRect(editor, x, y)) {
			editor.onClick && editor.onClick(x, y);
			return;
		}
	}
});

pointer.onRelease(function () {
	for (var i = 0; i < editors.length; i++) {
		var editor = editors[i];
		editor.onMouseUp && editor.onMouseUp();
	}
});

pointer.onMove(function (x, y) {
	if (mouse.x === x && mouse.y === y) return;
	mouse.x = x;
	mouse.y = y;
	if (popup) {
		popup.onMove && popup.onMove(x, y);
		return;
	}
	for (var i = 0; i < editors.length; i++) {
		var editor = editors[i];
		if (isInRect(editor, x, y)) {
			editor.onMove && editor.onMove(x, y);
			return;
		}
	}
});

pointer.onScroll(function (delta) {
	if (popup) {
		popup.onScroll && popup.onScroll(delta);
		return;
	}

	for (var i = 0; i < editors.length; i++) {
		var editor = editors[i];
		if (isInRect(editor, mouse.x, mouse.y)) {
			editor.onScroll && editor.onScroll(delta);
			return;
		}
	}
});

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
window.OVERSAMPLING = 1;

function incOversampling(inc) {
	window.OVERSAMPLING = clamp(window.OVERSAMPLING + inc, 1, 20);
	console.log('OVERSAMPLING = ', window.OVERSAMPLING)
}

exports.update = function () {
	if (popup) {
		popup.update && popup.update(); // TODO: not needed anymore.
		return;
	}

	// if (btnp.up)   incOversampling(1);
	// if (btnp.down) incOversampling(-1);

	if (btnp.A)   { program.setNote(state.sampleIndex); render(); }
	if (btnp.DEL) { program.deleteNote(); render(); }

	// TODO: currently, only sampleList is using it, refacrtor and remove
	for (var i = 0; i < editors.length; i++) {
		var editor = editors[i];
		editor.update && editor.update();
	}

	infoTip.update();
};


//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function render() {
	for (var i = 0; i < editors.length; i++) {
		var editor = editors[i];
		editor.render && editor.render();
	}
}

exports.render = render;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.openPopup = function (p) {
	if (popup) return console.error('Only one popup allowed');
	popup = p;
	popup.render && popup.render();
};

exports.closePopup = function () {
	popup = null;
	exports.render();
};
