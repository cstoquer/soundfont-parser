var state = require('../state');

var POSX   = 0;
var POSY   = 29;
var WIDTH  = 136;
var HEIGHT = 188;
var LINES  = 31;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var scroll = 0;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function printSound(index, i) {
	if (index < 0 || index >= state.samples.length) return;
	var y = i * 6 + POSY + 1;
	if (index === state.sampleIndex) {
		paper(14).rectf(POSX, y - 1, WIDTH, 7);
		sprite(48, POSX + 1, y - 1);
	}
	pen(1).print(state.samples[index].name, 12, y);
	pen(9).print(' (' + (index + 1) + '/' + state.samples.length + ')', 91, y);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function render() {
	paper(13).rectf(POSX, POSY, WIDTH, HEIGHT);
	if (state.samples.length === 0) {
		pen(15).print('drop sample here', 34, 100);
		print('(wav, mp3, sf2)', 36, 110);
		return;
	}
	for (var i = 0; i < LINES; i++) {
		printSound(~~scroll + i, i);
	}
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function changeSound(index) {
	state.sampleIndex = index;
	render();
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.rect = { left: POSX, right: POSX + WIDTH, top: POSY, bottom: POSY + HEIGHT };
exports.render = render;
exports.update = function () {
	if (btnp.down) changeSound((state.sampleIndex + 1) % state.samples.length);
	if (btnp.up)   changeSound((state.sampleIndex + state.samples.length - 1) % state.samples.length);
};

exports.onScroll = function (delta) {
	var prevScroll = ~~scroll;
	scroll -= delta / 120;
	scroll = clamp(scroll, 0, state.samples.length - LINES);
	if (~~scroll === prevScroll) return;
	render();
};

exports.onClick = function (x, y) {
	y -= POSY + 2;
	var i = ~~(y / 6) + ~~scroll;
	if (i < 0 || i >= state.samples.length) return;
	state.sampleIndex = i;
	render();
};
