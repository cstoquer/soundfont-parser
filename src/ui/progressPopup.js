var Sprite    = require('pixelbox/spritesheet/Sprite');
var NineSlice = require('pixelbox/NineSlice');

var PANEL = new NineSlice(new Sprite(assets.tilesheet, 'tilesheet',  9 * 8, 5 * 8, 8, 8), 4, 4);
var SLOT  = new NineSlice(new Sprite(assets.tilesheet, 'tilesheet', 11 * 8, 1 * 8, 8, 8), 4, 4);
var BAR   = new NineSlice(new Sprite(assets.tilesheet, 'tilesheet', 10 * 8, 5 * 8, 5, 5), 2, 2);

var message  = '';
var progress = 0;


//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function render () {
	PANEL.draw(100, 50, 200, 70);
	pen(1).print(message, 120, 80)
	SLOT.draw(120, 100, 160, 6);
	BAR.draw(121, 101, ~~(158 * progress), 6);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.setMessage  = function (m) {
	message = m;
	render();
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.setProgress = function (p) {
	progress = p;
	render();
};

exports.render = render;
