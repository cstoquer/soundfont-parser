var voiceEditor = require('./voiceEditor');
var piano       = require('../piano');
var state       = require('../state');
var program     = state.program;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function drawKeyboard() {
	for (var i = 0; i < 3; i++) {
		draw(assets.ui.keyboard, 136, i * 72);
	}
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function render() {
	paper(0).rectf(136, 0, 128, 217);
	drawKeyboard();
	program.render();
	sprite(64, 136 + 17, 211 - state.noteIndex * 6);
	voiceEditor.render();
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.rect = { left: 136, right: 264, top: 0, bottom: 217 };
exports.render = render;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var playingNote = -1;

exports.onClick = function (x, y) {
	var note = clamp(35 - ~~(y / 6), 0, 35);
	if (x <= 152) {
		playingNote = note;
		piano.noteOn(note);
	} else {
		state.noteIndex = note;
		render();
		return true;
	}
};

exports.onMouseUp = function () {
	if (playingNote === -1) return;
	piano.noteOff(playingNote);
	playingNote = -1;
}
