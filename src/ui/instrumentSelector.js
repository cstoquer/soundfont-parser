var state         = require('../state');
var programEditor = require('./programEditor');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function render() {
	paper(6).rectf(264, 0, 136, 34);
	draw(assets.ui.instruments, 267, 12);
	sprite(80, 270 + state.instrumentIndex * 13, 3);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.rect = { left: 264, right: 400, top: 0, bottom: 34 };
exports.onClick = function (x, y) {
	x -= 267;
	state.instrumentIndex = clamp(~~(x / 13), 0, 9);
	render();
	programEditor.render();
	return false;
};

exports.render = render;
