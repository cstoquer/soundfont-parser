var ButtonPool      = require('./components/ButtonPool');
var ui              = require('.');
var waveformView    = require('./waveformView');
var exportPetaporon = require('../exportPetaporon');
var persistency     = require('../persistency');
var state           = require('../state');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var POSX = 0;
var POSY = 0;
var WIDTH = 136;
var HEIGHT = 27;

var BG = getMap('menu');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function onWaveClic() {
	var sample = state.samples[state.sampleIndex];
	if (!sample) return;
	ui.openPopup(waveformView);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function onAssign() {
	state.program.setNote(state.sampleIndex);
	ui.render();
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var buttons = new ButtonPool();
buttons.addButton(POSX +   4, POSY + 7, 16, 16, exportPetaporon, 'export petaporon soundbank');
buttons.addButton(POSX +  20, POSY + 7, 16, 16, persistency.saveProgram, 'save soundbank');
// buttons.addButton(POSX +  44, POSY + 7, 16, 16, function () { exportPetaporon(true) }, 'export wav file');
buttons.addButton(POSX +  84, POSY + 7, 16, 16, onWaveClic, 'edit waveform loop points');
buttons.addButton(POSX + 116, POSY + 7, 16, 16, onAssign, 'assign sample to note');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.rect = { left: POSX, right: POSX + WIDTH, top: POSY, bottom: POSY + HEIGHT };
exports.render = function () {
	paper(4).rectf(POSX, POSY, WIDTH, HEIGHT);
	draw(BG, POSX, POSY + 3);
	draw(assets.ui.separator, POSX, POSY + HEIGHT);
	// buttons.debug();
};
exports.onClick = buttons.click.bind(buttons);
exports.onMove  = buttons.onMove.bind(buttons);