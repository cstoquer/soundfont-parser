var state      = require('./state');
var soundfonts = require('./data/soundfonts');
var Sample     = require('./data/Sample');
var FileSaver  = require('file-saver');
var prettyJson = require('json-stringify-pretty-compact');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function serialize() {
	var samples = [];

	for (var i = 0; i < state.samples.length; i++) {
		samples.push(state.samples[i].serialize());
	}

	return {
		samples: samples,
		program: state.program.serialize()
	};
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function deserialize(data) {
	soundfonts.clearCache();

	state.samples         = [];
	state.sampleIndex     = 0;
	state.instrumentIndex = 0;
	state.noteIndex       = 0;

	for (var i = 0; i < data.samples.length; i++) {
		var sample = new Sample();
		sample.deserialize(data.samples[i]);
		state.samples.push(sample);
	}

	// TODO: wait for all samples to load ?

	state.program.deserialize(data.program);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function saveProgram() {
	var data = serialize();
	var text = prettyJson(data, { maxLength: 100, indent: '\t' });
	var blob = new Blob([text], { type: 'text/plain;charset=utf-8' });
	FileSaver.saveAs(blob, 'program.petaprog');
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function loadProgram(file) {
	// var data = fs.readFileSync(uri, 'utf8');
	// deserialize(JSON.parse(data));
	var reader = new FileReader();
	reader.onload = function (e) {
		var data = e.target.result;
		deserialize(JSON.parse(data));
	};
	reader.readAsText(file);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.serialize   = serialize;
exports.deserialize = deserialize;
exports.saveProgram = saveProgram;
exports.loadProgram = loadProgram;
