var audioContext  = require('./engine/audioContext');
var state         = require('./state');
var program       = state.program;
var FileSaver     = require('file-saver');
var prettyJson    = require('json-stringify-pretty-compact');
var ui            = require('./ui');
var progressPopup = require('./ui/progressPopup');


var SAMPLE_PADDING = 241;
var FADE_PADDING   = 200;
var TOTAL_PADDING  = SAMPLE_PADDING + FADE_PADDING;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function onProgress(progress) {
	progressPopup.setProgress(progress);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function exportPetaporon(exportWave) {
	ui.openPopup(progressPopup);
	progressPopup.setMessage('exporting program');

	var result = {
		n: 'NAME',
		a: 'audio/samples.mp3',
		s: [], // {"n": "0-xylophone", "s": 1244, "e": 76913, "l": 0},
		i: [], // {"s":[{"a":0, "s":0,"e":35,"p": 0,"v":0.7,"n":[0.330, 0.103, 0.900, 0.002, 1]}]}
	};

	var samples    = [];
	var samplesMap = {};

	function getNoteSample(note) {
		var sample = note.sample;
		var id = sample.id;
		if (samplesMap[id]) return samplesMap[id];

		var sampleInfo = {
			index: samples.length,
			sample: note.sample
		};

		samples.push(sampleInfo);
		samplesMap[id] = sampleInfo;
		return sampleInfo;
	}

	// instruments
	var instruments = program.instruments;
	for (var i = 0; i < instruments.length; i++) {
		var instrument = instruments[i];
		var notes = instrument.notes;

		var sounds = [];
		result.i.push({ s: sounds });
		var previousSound = null;

		for (var n = 0; n < notes.length; n++) {
			var note = notes[n];
			if (!note) continue;

			var sound = {
				a: getNoteSample(note).index,
				s: note.index,
				e: 35, // end index = next note index or 35
				p: note.pitch,
				v: note.volume / 10,
				n: note.getEnvelopeArray(),
			};
			if (note.lfo) {
				sound.l = note.getLfoArray();
			}
			sounds.push(sound);
			if (previousSound) {
				previousSound.e = note.index - 1;
			}
			previousSound = sound;
		}
	}

	// samples
	var head = 1105; // mp3 compression create a 1105 sample blank at the beginning
	for (var s = 0; s < samples.length; s++) {
		var sample = samples[s].sample;
		var endPoint = sample.end - sample.start + head;

		if (sample.loop > 0) {
			// shift end point few samples to avoid problem with small buffers
			endPoint += 100;
		}

		result.s.push({
			n: sample.name,
			s: head,
			e: endPoint,
			l: sample.loop
		});

		head += sample.end - sample.start + TOTAL_PADDING;
	}

	// buffer
	var audioBuffer = audioContext.createBuffer(1, head, 44100);
	var channelData = audioBuffer.getChannelData(0);
	var head = 0;
	for (var s = 0; s < samples.length; s++) {
		var sample    = samples[s].sample;
		var audioData = sample.channelData;
		var length    = sample.length;
		var fadeAt    = sample.end + FADE_PADDING;
		var fade      = 1;

		if (sample.loop === 0) {
			for (var i = sample.start; i < sample.end + TOTAL_PADDING; i++) {
				if (i > fadeAt) fade = 1 - (i - fadeAt) / FADE_PADDING;
				channelData[head++] = fade * (i < length ? audioData[i] : 0);
			}
		} else {
			var pos = sample.start;
			for (var i = sample.start; i < sample.end + TOTAL_PADDING; i++) {
				if (i > fadeAt) fade = 1 - (i - fadeAt) / FADE_PADDING;
				channelData[head++] = fade * audioData[pos++];
				if (pos >= sample.end) pos -= sample.loop;
			}
		}
	}

	// encode mp3 or wav
	var compressionRate = exportWave ? 0 : 128;
	progressPopup.setMessage('Creating audio');
	audioEncoder(audioBuffer, compressionRate, onProgress, function onComplete(blob) {
		if (exportWave) {
			FileSaver.saveAs(blob, 'instrument.wav');
			ui.closePopup();
			return;
		}

		// create zip
		progressPopup.setMessage('Building archive')
		var zip = new JSZip();
		zip.file('samples.mp3', blob);
		zip.file('data.json', prettyJson(result, { maxLength: 120, indent: '\t' }));

		// save file
		zip.generateAsync({ type: 'blob' }).then(function (content) {
			FileSaver.saveAs(content, 'instrument.zip');
			ui.closePopup();
		});
	});
}

module.exports = exportPetaporon;
