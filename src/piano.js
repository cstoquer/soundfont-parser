var MIDI    = require('./engine/MIDI');
var synth   = require('./synth');
var state   = require('./state');
var program = state.program;


//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function noteOn(note, velocity) {
	velocity = velocity || 127;
	velocity /= 127;
	if (note < 0 || note > 35) return;
	var sound = program.getSound(note);
	if (!sound) return;
	synth.noteOn(note, sound, velocity);
}

function noteOff(note) {
	if (note < 0 || note > 35) return;
	synth.noteOff(note);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
MIDI.open(function onOpen() {
	console.log('MIDI >>>>>>', MIDI)
});

MIDI.on('message', function (e) {
	switch (e.midiType) {
		// case 'note on':  noteOn (e.note - 48, e.velocity); break;
		case 'note on':  noteOn (e.note - 48, 127); break;
		case 'note off': noteOff(e.note - 48); break;
	}
});

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.noteOn  = noteOn;
exports.noteOff = noteOff;
