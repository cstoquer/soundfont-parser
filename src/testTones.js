var noteUtils    = require('./synth/noteUtils');
var audioContext = require('./engine/audioContext');
var synth        = require('./synth');
var TAU          = Math.PI * 2;
var PITCHES      = [0, 2, 4, 5, 7, 9, 11];

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function createToneBuffer() {
	var freq = noteUtils.noteToFreq(60); // C
	var bufferLength = Math.round(44100 / freq);

	var audioBuffer = audioContext.createBuffer(1, bufferLength, 44100);
	var channelData = audioBuffer.getChannelData(0);

	for (var i = 0; i < bufferLength; i++) {
		channelData[i] = Math.sin(i * TAU / bufferLength);
	}

	return channelData;
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function createNotes() {
	var buffer = createToneBuffer();
	var notes = [];

	for (var i = 0; i < 7; i++) {
		var sound = {
			pitch:  60 + PITCHES[i],
			buffer: buffer,
			vol:    1,
			sample: { start: 0, end: buffer.length, loop: buffer.length },
			env:    [0.04, 0.01, 0.9, 0.01, 0], // [A, D, S, R, P]
			perc:   false,
			lfo:    null,
			hasLfo: false,
		};

		notes.push(sound);
	}

	return notes;
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var NOTES = createNotes();

var currentIndex = -1;

exports.noteOn = function (index) {
	if (currentIndex !== -1) return;
	currentIndex = index;
	synth.noteOn(currentIndex, NOTES[index], 1);
};

exports.noteOff = function () {
	if (currentIndex === -1) return;
	synth.noteOff(currentIndex);
	currentIndex = -1;
};
